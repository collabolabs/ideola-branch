Vue.component('product-services', {
	template: `
	<section class="section section-padding-equal bg-color-dark">
		<div class="container">
			<div class="section-heading heading-light-left">
				<span class="subtitle">What We Can Do For You</span>
				<h2 class="title">Services we can help you with</h2>
				<p class="opacity-50">Nulla facilisi. Nullam in magna id dolor
					blandit rutrum eget vulputate augue sed eu imperdiet.</p>
			</div>
			<div class="row">
				<div v-for="(service, index) in services" class="col-lg-4 col-md-6" data-sal="slide-up" data-sal-duration="800" :data-sal-delay="service.delay">
					<div class="services-grid active">
						<div class="thumbnail">
							<img :src="service.imglink" alt="icon">
						</div>
						<div class="content">
							<h5 class="title"> <a :href="service.urllink">{{ service.title }}</a></h5>
							<p>Simply drag and drop photos and videos into your workspace to automatically add them to your Collab Cloud library {{index}}.</p>
							<a :href="service.urllink" class="more-btn">Find out more</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<ul class="list-unstyled shape-group-10">
			<li class="shape shape-1"><img src="assets/media/others/circle-1.png" alt="Circle"></li>
			<li class="shape shape-2"><img src="assets/media/others/line-3.png" alt="Circle"></li>
			<li class="shape shape-3"><img src="assets/media/others/bubble-5.png" alt="Circle"></li>
		</ul>
	</section>
	`,
	data() {
		return {
			address: "Simo Gunung Barat Tol 2/11",
			country: "Surabaya, Jawa Timur",
			services: [
				{
					title: "Design",
					imglink: "assets/media/icon/icon-1.png",
					urllink: "service-design.html",
					delay: 100
				},
				{
					title: "Development",
					imglink: "assets/media/icon/icon-2.png",
					urllink: "service-development.html",
					delay: 200,
				},
				{
					title: "Digital marketing",
					imglink: "assets/media/icon/icon-3.png",
					urllink: "service-marketing.html",
					delay: 300
				},
				{
					title: "Business",
					imglink: "assets/media/icon/icon-4.png",
					urllink: "service-business.html",
					delay: 100
				},
				{
					title: "Website Development",
					imglink: "assets/media/icon/icon-5.png",
					urllink: "service-technology.html",
					delay: 200
				},
				{
					title: "Photography",
					imglink: "assets/media/icon/icon-6.png",
					urllink: "service-content-strategy.html",
					delay: 300
				}
			],
		}
	}
})

var app = new Vue({
	el: "#main-wrapper",
	data: {
		address: "Simo Gunung Barat Tol 2/11",
		country: "Surabaya, Jawa Timur, Indonesia",
		methods: {
			
		},
		computed: {
		
		}
	}
})